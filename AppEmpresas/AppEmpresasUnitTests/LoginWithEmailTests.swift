//
//  LoginWithEmailTests.swift
//  AppEmpresasUnitTests
//
//  Created by Isnard Silva on 23/12/20.
//

import XCTest
@testable import AppEmpresas

class LoginWithEmailTests: XCTestCase {
    // MARK: - Properties
    var sut: LoginWithEmail!
    
    
    // MARK: - Setup Methods
    override func setUp() {
        super.setUp()
    }
    
    override func tearDown() {
        super.tearDown()
        sut = nil
    }
    
    
    // MARK: - Test Methods
    func testLoginWithSuccess() {
        // Given
        let validEmail = IoasysAPISources.FakeUser.email
        let validPassword = IoasysAPISources.FakeUser.password
        var expectedResponse: AuthCredentials?
        let promise = expectation(description: "Test Login with email and password")
        
        let loginWithEmail = LoginWithEmail(email: validEmail, password: validPassword)
        
        // When
        loginWithEmail.authenticate(completionHandler: { result in
            // Then
            switch result {
            case .failure(let error):
                XCTFail(error.localizedDescription)
                
            case .success(let authCredentials):
                expectedResponse = authCredentials
            }
            
            promise.fulfill()
        })
        wait(for: [promise], timeout: 5)
        
        
        // Then
        XCTAssertNotNil(expectedResponse)
        XCTAssertEqual(expectedResponse?.uid, validEmail)
    }
    
    func testLoginWithWrongPassword() {
        // Given
        let validEmail = IoasysAPISources.FakeUser.email
        let validPassword = IoasysAPISources.FakeUser.password + "1"
        let expectedError: LoginWithEmailError = .wrongEmailOrPassword
        var receivedError: Error?
        let promise = expectation(description: "Test Login with email and password")
        
        let loginWithEmail = LoginWithEmail(email: validEmail, password: validPassword)
        
        // When
        loginWithEmail.authenticate(completionHandler: { result in
            // Then
            switch result {
            case .failure(let error):
                receivedError = error
                
            case .success:
                break
            }
            
            promise.fulfill()
        })
        wait(for: [promise], timeout: 5)
        
        
        // Then
        guard let detectedError = receivedError as? LoginWithEmailError else {
            XCTFail("The format of the detected error is invalid.")
            return
        }
        
        XCTAssertEqual(detectedError, expectedError)
    }
}
