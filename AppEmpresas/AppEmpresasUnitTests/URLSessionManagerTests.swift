//
//  URLSessionManagerTests.swift
//  AppEmpresasUnitTests
//
//  Created by Isnard Silva on 22/12/20.
//

import XCTest
@testable import AppEmpresas

class URLSessionManagerTests: XCTestCase {
    // MARK: - Properties
    var sut: URLSessionManager!
    
    
    // MARK: - Setup Methods
    override func setUp() {
        super.setUp()
        sut = URLSessionManager()
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    
    // MARK: - Test Methods
    func testRequestForAnURL() {
        // Given
        let fakeService = FakeService.fetchData
        var receivedError: NetworkError?
        let promise = expectation(description: "Call for an API")
        
        // When
        sut.request(service: fakeService, completionHandler: { result in
            switch result {
            case .failure(let detectedError):
                receivedError = detectedError
            case .success:
                break
            }
            promise.fulfill()
        })
        wait(for: [promise], timeout: 5)
        
        // Then
        XCTAssertNil(receivedError)
    }
}

