//
//  FakeService.swift
//  AppEmpresasUnitTests
//
//  Created by Isnard Silva on 22/12/20.
//

import Foundation
@testable import AppEmpresas

/// Serviço fake para testar se as implementações da camada de Network está funcionado
enum FakeService {
    case fetchData
}


// MARK: - ServiceProtocol
extension FakeService: ServiceProtocol {
    var path: String {
        return "https://jsonplaceholder.typicode.com/todos/1"
    }
    
    var method: HTTPMethod {
        return .GET
    }
    
    var parameters: [String : Any]? {
        return nil
    }
    
    var headers: [String : String]? {
        return nil
    }
}
