//
//  AlertManager.swift
//  AppEmpresas
//
//  Created by Isnard Silva on 24/12/20.
//

import UIKit

class AlertManager {
    
    // MARK: - Alert Types
    func createDefaultAlert(title: String, message: String, buttonName: String, completionHandler: (() -> Void)? = nil) -> UIAlertController {
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: buttonName, style: .default, handler: { _ in
            completionHandler?()
        }))
        
        return alert
    }
}
