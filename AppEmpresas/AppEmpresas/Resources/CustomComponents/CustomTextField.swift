//
//  CustomTextField.swift
//  AppEmpresas
//
//  Created by Isnard Silva on 24/12/20.
//

import UIKit

class CustomTextField: UITextField {
    // MARK: - Views
    let placeholderLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor(named: Identifier.Color.fontGray)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
        
    
    // MARK: - Properties
    
    
    // MARK: - Initialization
    init() {
        super.init(frame: .zero)
        setupViews()
        setupCustomUI()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    // MARK: - Setup Custom UI
    private func setupCustomUI() {
        backgroundColor = UIColor(named: Identifier.Color.backgroundTextField)
    }
}

// MARK: - ViewCodable
extension CustomTextField: ViewCodable {
    func setupHierarchy() {
        addSubview(placeholderLabel)
    }
    
    func setupConstraints() {
        NSLayoutConstraint.activate([
            self.heightAnchor.constraint(equalToConstant: 48)
        ])
        
        NSLayoutConstraint.activate([
            placeholderLabel.leadingAnchor.constraint(equalTo: self.leadingAnchor),
            placeholderLabel.trailingAnchor.constraint(equalTo: self.trailingAnchor),
            placeholderLabel.bottomAnchor.constraint(equalTo: self.topAnchor, constant: -4)
        ])
    }
    
    func setupAditionalConfiguration() {
        self.borderStyle = .roundedRect
        self.translatesAutoresizingMaskIntoConstraints = false
    }
}


// MARK: - Setup Custom UI
extension CustomTextField {
    
}
