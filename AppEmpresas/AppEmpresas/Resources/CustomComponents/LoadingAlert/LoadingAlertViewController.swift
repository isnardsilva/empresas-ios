//
//  LoadingAlertViewController.swift
//  AppEmpresas
//
//  Created by Isnard Silva on 25/12/20.
//

import UIKit

class LoadingAlertViewController: UIViewController {
    // MARK: - Properties
    private lazy var baseView = LoadingAlertView()
    
    // MARK: - Initialization
    init() {
        super.init(nibName: nil, bundle: nil)
        self.modalPresentationStyle = .overCurrentContext
        self.modalTransitionStyle = .crossDissolve
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    // MARK: - View Life Cycle
    override func loadView() {
        super.loadView()
        self.view = baseView
    }
}
