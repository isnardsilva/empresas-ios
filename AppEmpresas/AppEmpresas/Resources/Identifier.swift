//
//  Identifier.swift
//  AppEmpresas
//
//  Created by Isnard Silva on 22/12/20.
//

import Foundation

/// Armazena todas as strings utilizadas em meio ao projeto
enum Identifier {
    enum Cell {
        static let enterpriseCell = "enterpriseCell"
        static let enterpriseHeaderReusableView = "enterpriseHeaderReusableView"
    }
    
    enum DatabaseKeys {
        static let authCredentials = "authCredentials"
    }
    
    enum Color {
        static let backgroundTextField = "background_text_field"
        static let fontGray = "font_gray"
        static let customPink = "custom_pink"
    }
    
    enum Image {
        static let topBarLogin = "top_bar_login"
        static let topBarLargeEnterpriseList = "top_bar_large_enterprise_list"
        static let topBarShortEnterpriseList = "top_bar_short_enterprise_list"
        static let logoWithoutName = "logo_without_name"
        static let backButton = "back_button"
    }
}
