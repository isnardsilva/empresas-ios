//
//  ViewCodable.swift
//  AppEmpresas
//
//  Created by Isnard Silva on 24/12/20.
//

import Foundation

protocol ViewCodable {
    func setupViews()
    func setupHierarchy()
    func setupConstraints()
    func setupAditionalConfiguration()
}


// MARK: Default Implementations
extension ViewCodable {
    func setupViews() {
        setupHierarchy()
        setupConstraints()
        setupAditionalConfiguration()
    }
}
