//
//  UIImageView+SetImage.swift
//  AppEmpresas
//
//  Created by Isnard Silva on 23/12/20.
//

import UIKit
import Kingfisher

extension UIImageView {
    /// Permite exibir uma imagem baixada da internet (a partir de um link) no Image View
    func setImage(url: URL?) {
        if url == nil {
            self.image = UIImage(systemName: "photo")
            self.contentMode = .scaleAspectFit
            return
        } else {
            self.kf.indicatorType = .activity
            self.kf.setImage(with: url)
        }
    }
}
