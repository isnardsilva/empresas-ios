//
//  LoginWithEmailError.swift
//  AppEmpresas
//
//  Created by Isnard Silva on 23/12/20.
//

import Foundation

/// Conjunto de erros que podem ser tratados ao realizar o login com e-mail e senha
enum LoginWithEmailError: Error {
    case responseNotDecoded
    case invalidServerResponse
    case wrongEmailOrPassword
    case emptyAccessToken
    case emptyEmail
    case emptyPassword
    case unknowError
}


// MARK: - LocalizedError
extension LoginWithEmailError: LocalizedError {
    var errorDescription: String? {
        switch self {
        case .responseNotDecoded:
            return "Não foi possível decodificara resposta obtida do servidor."
        case .invalidServerResponse:
            return "A resposta obtida do servidor é inválida."
        case .wrongEmailOrPassword:
            return "E-mail ou senha estão incorretos."
        case .emptyAccessToken:
            return "Não foi possível obter o Token de acesso."
        case .emptyEmail:
            return "Insira o e-mail"
        case .emptyPassword:
            return "Insira a senha"
        case .unknowError:
            return "Não foi possível realizar o seu login. Tente novamente."
        }
    }
}
