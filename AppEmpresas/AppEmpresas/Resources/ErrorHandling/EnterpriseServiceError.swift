//
//  EnterpriseServiceError.swift
//  AppEmpresas
//
//  Created by Isnard Silva on 23/12/20.
//

import Foundation

enum EnterpriseServiceError: Error {
    case responseNotDecoded
    case unknowError
}


// MARK: - LocalizedError
extension EnterpriseServiceError: LocalizedError {
    var errorDescription: String? {
        switch self {
        case .responseNotDecoded:
            return "Não foi possível decodificar os dados obtidos do servidor."
        case .unknowError:
            return "Houve algum erro na busca pelas empresas. Tente novamente."
        }
    }
}
