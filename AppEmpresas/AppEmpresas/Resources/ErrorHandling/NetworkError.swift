//
//  NetworkError.swift
//  AppEmpresas
//
//  Created by Isnard Silva on 22/12/20.
//

import Foundation

/// Conjunto de erros que podem ser tratados ao realizar uma conexão com a internet
enum NetworkError: Error {
    case invalidURL
    case offline
    case connectionError
    case responseWithoutData
    case invalidResponseType
    case unknowError
}


// MARK: - LocalizedError
extension NetworkError: LocalizedError {
    var errorDescription: String? {
        switch self {
        case .invalidURL:
            return "A URL passada é inválida."
        case .offline:
            return "Não há conexão com a internet."
        case .connectionError:
            return "Houve algum problema no processo de conexão com o servidor."
        case .responseWithoutData:
            return "A resposta obtida do servidor não possui dados."
        case .invalidResponseType:
            return "A resposta obtida do servidor é inválida."
        case .unknowError:
            return "Houve um error desconhecido."
        }
    }
}
