//
//  AuthenticationError.swift
//  AppEmpresas
//
//  Created by Isnard Silva on 23/12/20.
//

import Foundation

enum AuthenticationError {
    case sessionExpired
}

// MARK: - LocalizedError
extension AuthenticationError: LocalizedError {
    var errorDescription: String? {
        switch self {
        case .sessionExpired:
            return "A sua sessão expirou. Faça login novamente!"
        }
    }
}
