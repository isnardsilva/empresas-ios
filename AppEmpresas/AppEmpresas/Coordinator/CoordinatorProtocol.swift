//
//  CoordinatorProtocol.swift
//  AppEmpresas
//
//  Created by Isnard Silva on 22/12/20.
//

import UIKit

/// Protocolo que determina de que maneiras as implementações do Coordinator Pattern devem trabalhar
protocol CoordinatorProtocol {
    var navigationController: UINavigationController { get set }
    func start()
}
