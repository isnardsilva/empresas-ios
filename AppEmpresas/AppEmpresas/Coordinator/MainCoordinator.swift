//
//  MainCoordinator.swift
//  AppEmpresas
//
//  Created by Isnard Silva on 22/12/20.
//

import UIKit

/// Implementação central do Coordinator Pattern presente no projeto
class MainCoordinator: CoordinatorProtocol {
    // MARK: - Properties
    var navigationController: UINavigationController
    
    
    // MARK: - Initialization
    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }
    
    
    // MARK: - Start Method
    func start() {
        // Check if user is authenticated
        let loginDAO = LoginDAO()
        
        if loginDAO.getAuthCredentials() != nil {
            self.navigateToEnterpriseList()
        } else {
            self.navigateToLogin()
        }
    }
}


// MARK: - Navigation
extension MainCoordinator {
    func navigateToLogin() {
        let loginVC = LoginViewController()
        loginVC.coordinator = self
        
        if navigationController.viewControllers.isEmpty {
            navigationController.pushViewController(loginVC, animated: true)
        } else {
            navigationController.viewControllers.insert(loginVC, at: 0)
            navigationController.popToRootViewController(animated: true)
        }
    }
    
    func navigateToEnterpriseList() {
        let enterpriseListVC = EnterpriseListViewController()
        enterpriseListVC.coordinator = self
        
        if navigationController.viewControllers.isEmpty {
            navigationController.pushViewController(enterpriseListVC, animated: true)
        } else {
            navigationController.viewControllers.insert(enterpriseListVC, at: 0)
            navigationController.popToRootViewController(animated: true)
        }
    }
    
    func navigateToEnterpriseDetail(enterprise: Enterprise) {
        let enterpriseDetailVC = EnterpriseDetailViewController(enterprise: enterprise)
        enterpriseDetailVC.coordinator = self
        navigationController.pushViewController(enterpriseDetailVC, animated: true)
    }
}
