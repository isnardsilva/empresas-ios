//
//  LoginWithEmail.swift
//  AppEmpresas
//
//  Created by Isnard Silva on 23/12/20.
//

import Foundation

class LoginWithEmail {
    // MARK: - Properties
    private var networkManager: NetworkManagerProtocol
    private var email: String
    private var password: String
    
    
    // MARK: - Initialization
    init(email: String, password: String, networkManager: NetworkManagerProtocol = URLSessionManager()) {
        self.email = email
        self.password = password
        self.networkManager = networkManager
    }
    
}


// MARK: - LoginProtocol
extension LoginWithEmail: LoginProtocol {
    
    func authenticate(completionHandler: @escaping (Result<AuthCredentials, Error>) -> Void) {
        
        guard !email.isEmpty else {
            completionHandler(.failure(LoginWithEmailError.emptyEmail))
            return
        }
        
        guard !password.isEmpty else {
            completionHandler(.failure(LoginWithEmailError.emptyPassword))
            return
        }
        
        
        let serviceInfo = LoginServiceInfo.authenticateWithEmail(email: email, password: password)
        
        networkManager.request(service: serviceInfo, completionHandler: { result in
            switch result {
            case .failure(let error):
                completionHandler(.failure(error))
                
            case .success(let networkResponse):
                do {
                    let authCredential = try self.handleAuthenticationResponse(networkResponse: networkResponse)
                    completionHandler(.success(authCredential))
                } catch {
                    completionHandler(.failure(error))
                }
            }
        })
    }
}


// MARK: - Handle Authentication Response
extension LoginWithEmail {
    private func handleAuthenticationResponse(networkResponse: NetworkResponse) throws -> AuthCredentials {
        // Get API Response
        guard let apiResponse = try? JSONDecoder().decode(IoasysAPIAuthResponse.self, from: networkResponse.receivedData) else {
            throw LoginWithEmailError.responseNotDecoded
        }
        
        // Check response is success
        guard apiResponse.success == true else {
            let statusCode = networkResponse.httpResponse.statusCode
            if statusCode == IoasysAPISources.ErrorStatusCode.invalidLoginCredentials {
                throw LoginWithEmailError.wrongEmailOrPassword
            } else {
                throw LoginWithEmailError.unknowError
            }
        }
        
        
        // Check Headers Fields
        // Get Authentication Credentials
        guard let authCredentials = self.getAuthCredentials(networkResponse: networkResponse) else {
            throw LoginWithEmailError.emptyAccessToken
        }
        
        return authCredentials
    }
    
    private func getAuthCredentials(networkResponse: NetworkResponse) -> AuthCredentials? {
        // All HeaderFields
        let headerFields = networkResponse.httpResponse.allHeaderFields
        
        // Get specific fields
        guard let accessToken = headerFields[IoasysAPISources.ParameterName.accessToken],
              let uid = headerFields[IoasysAPISources.ParameterName.uid],
              let client = headerFields[IoasysAPISources.ParameterName.client] else {
            return nil
        }
        
        let authCredentials = AuthCredentials(accessToken: String(describing: accessToken),
                                              uid: String(describing: uid),
                                              client: String(describing: client))
        
        return authCredentials
    }
}
