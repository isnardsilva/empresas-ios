//
//  LoginService.swift
//  AppEmpresas
//
//  Created by Isnard Silva on 22/12/20.
//

import Foundation

enum LoginServiceInfo {
    // Aqui eu passo um login protocol
    case authenticateWithEmail(email: String, password: String)
}

extension LoginServiceInfo: ServiceProtocol {
    var path: String {
        switch self {
        case .authenticateWithEmail:
            return IoasysAPISources.baseURL + IoasysAPISources.authExtensionURL
        }
    }
    
    var method: HTTPMethod {
        switch self {
        case .authenticateWithEmail:
            return .POST
        }
    }
    
    var parameters: [String : Any]? {
        switch self {
        case .authenticateWithEmail(let email, let password):
            return [
                IoasysAPISources.ParameterName.email: email,
                IoasysAPISources.ParameterName.password: password
            ]
        }
    }
    
    var headers: [String : String]? {
        switch self {
        case .authenticateWithEmail:
            return nil
        }
    }
}
