//
//  LoginProtocol.swift
//  AppEmpresas
//
//  Created by Isnard Silva on 23/12/20.
//

import Foundation

/// Determina de que forma os métodos de Login deve se comportar
protocol LoginProtocol {
    func authenticate(completionHandler: @escaping (Result<AuthCredentials, Error>) -> Void)
}
