//
//  EnterpriseService.swift
//  AppEmpresas
//
//  Created by Isnard Silva on 23/12/20.
//

import Foundation

class EnterpriseService {
    // MARK: - Properties
    private let networkManager: NetworkManagerProtocol
    
    
    // MARK: - Initialization
    init(networkManager: NetworkManagerProtocol = URLSessionManager()) {
        self.networkManager = networkManager
    }
    
    
    // MARK: - Fetch Methods
    func searchByName(name: String, completionHandler: @escaping ((Result<[Enterprise], Error>) -> Void)) {
        let serviceInfo = EnterpriseServiceInfo.searchByName(name)
        
        
        networkManager.request(service: serviceInfo, completionHandler: { [weak self] result in
            switch result {
            case .failure(let error):
                completionHandler(.failure(error))
                
            case .success(let receivedResponse):
                do {
                    let enterprises = try self?.handleServeResponse(networkResponse: receivedResponse)
                    completionHandler(.success(enterprises ?? []))
                } catch {
                    completionHandler(.failure(error))
                }
            }
        })
    }
}

// MARK: - Handle Server Response
extension EnterpriseService {
    private func handleServeResponse(networkResponse: NetworkResponse) throws -> [Enterprise] {
        
        // Check status code
        let statusCode = networkResponse.httpResponse.statusCode
        guard (200...299).contains(statusCode) else {
            if statusCode == IoasysAPISources.ErrorStatusCode.invalidLoginCredentials {
                throw AuthenticationError.sessionExpired
            } else {
                throw EnterpriseServiceError.unknowError
            }
        }
        
        // Get data
        guard let receivedResponse = try? JSONDecoder().decode(IoasysAPIEnterpriseListResponse.self, from: networkResponse.receivedData) else {
            throw EnterpriseServiceError.responseNotDecoded
        }
        
        return receivedResponse.enterprises
    }
}
