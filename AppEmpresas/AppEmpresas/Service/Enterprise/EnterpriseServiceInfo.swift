//
//  EnterpriseServiceInfo.swift
//  AppEmpresas
//
//  Created by Isnard Silva on 23/12/20.
//

import Foundation

enum EnterpriseServiceInfo {
    case searchByName(_ name: String)
}


// MARK: - ServiceProtocol
extension EnterpriseServiceInfo: ServiceProtocol {
    var path: String {
        switch self {
        case .searchByName:
            return IoasysAPISources.baseURL + IoasysAPISources.enterprisesExtensionURL
        }
    }
    
    var method: HTTPMethod {
        switch self {
        case .searchByName:
            return .GET
        }
    }
    
    var parameters: [String : Any]? {
        switch self {
        case .searchByName(let name):
            return [
                IoasysAPISources.ParameterName.name: name
            ]
        }
    }
    
    var headers: [String : String]? {
        switch self {
        case .searchByName:
            return getHeadersWithAuthCredentials()
        }
    }
}

// MARK: - Helper Methods
extension EnterpriseServiceInfo {
    private func getHeadersWithAuthCredentials() -> [String: String]? {
        guard let authCredentials = LoginDAO().getAuthCredentials() else {
            return nil
        }
        
        let headersWithAuth = [
            IoasysAPISources.ParameterName.accessToken: authCredentials.accessToken,
            IoasysAPISources.ParameterName.client: authCredentials.client,
            IoasysAPISources.ParameterName.uid: authCredentials.uid
        ]
        
        return headersWithAuth
    }
}
