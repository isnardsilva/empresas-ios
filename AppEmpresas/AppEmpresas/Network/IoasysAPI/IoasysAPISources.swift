//
//  IoasysAPISources.swift
//  AppEmpresas
//
//  Created by Isnard Silva on 22/12/20.
//

import Foundation

/// Fornece um conjunto de informações como URLs, nomes dos parâmetros usados nas requests, entre outras informações que serão úteis no processo de conexão com a API da ioasys
enum IoasysAPISources {
    // MARK: - URLs
    static let baseURL = "https://empresas.ioasys.com.br"
    static let authExtensionURL = "/api/v1/users/auth/sign_in"
    static let enterprisesExtensionURL = "/api/v1/enterprises"
    
    
    // MARK: Parameter Name
    enum ParameterName {
        static let email = "email"
        static let password = "password"
        static let accessToken = "access-token"
        static let client = "client"
        static let uid = "uid"
        static let name = "name"
    }
    
    
    enum ErrorStatusCode {
        static let invalidLoginCredentials = 401
    }
    
    // MARK: - Fake User
    enum FakeUser {
        static let email = "testeapple@ioasys.com.br"
        static let password = "12341234"
    }
}
