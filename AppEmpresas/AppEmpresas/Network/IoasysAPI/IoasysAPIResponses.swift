//
//  IoasysAPIResponses.swift
//  AppEmpresas
//
//  Created by Isnard Silva on 22/12/20.
//

import Foundation

struct IoasysAPIAuthResponse: Codable {
    // MARK: - Properties
    let success: Bool
    let errors: [String]?
    
    // MARK: - Parse
    enum CodingKeys: String, CodingKey {
        case success, errors
    }
}

struct IoasysAPIEnterpriseListResponse: Codable {
    // MARK: - Properties
    let enterprises: [Enterprise]
    
    // MARK: - Parse
    enum CodingKeys: String, CodingKey {
        case enterprises
    }
}


