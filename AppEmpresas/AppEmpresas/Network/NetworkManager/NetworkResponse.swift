//
//  NetworkResponse.swift
//  AppEmpresas
//
//  Created by Isnard Silva on 22/12/20.
//

import Foundation

/// Especifica de que forma os dados recebidos do servidor deverão ser trabalhados
struct NetworkResponse {
    let httpResponse: HTTPURLResponse
    let receivedData: Data
}
