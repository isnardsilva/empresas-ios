//
//  NetworkManagerProtocol.swift
//  AppEmpresas
//
//  Created by Isnard Silva on 22/12/20.
//

import Foundation

/// Protocolo que especifica de que forma as framework de conexão entre o device e as API devem ser implementadas
protocol NetworkManagerProtocol {
    /// Realiza uma request para um link específico na Internet
    func request(service: ServiceProtocol, completionHandler: @escaping (Result<NetworkResponse, NetworkError>) -> Void)
}
