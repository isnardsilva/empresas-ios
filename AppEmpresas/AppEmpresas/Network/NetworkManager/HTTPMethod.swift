//
//  HTTPMethod.swift
//  AppEmpresas
//
//  Created by Isnard Silva on 22/12/20.
//

import Foundation

/// Especifica o tipo de requisição que será feita
enum HTTPMethod: String {
    case GET
    case POST
}
