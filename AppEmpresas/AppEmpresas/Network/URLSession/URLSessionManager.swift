//
//  URLSessionManager.swift
//  AppEmpresas
//
//  Created by Isnard Silva on 22/12/20.
//

import Foundation

/// Implementação do URLSession para se fazer a conexão entre o device e a internet
class URLSessionManager: NetworkManagerProtocol {
    func request(service: ServiceProtocol, completionHandler: @escaping (Result<NetworkResponse, NetworkError>) -> Void) {
        
        // Format URL
        guard let validURL = self.formatURL(service: service) else {
            completionHandler(.failure(.invalidURL))
            return
        }
        
        // Setup Request Type
        let urlRequest = self.setupURLRequest(url: validURL, service: service)
        
        // Run Request
        URLSession.shared.dataTask(with: urlRequest, completionHandler: { data, response, error in
            // Check offline error or unknow erros
            if let detectedError = error as NSError? {
                if detectedError.code == NSURLErrorDataNotAllowed {
                    completionHandler(.failure(.offline))
                } else {
                    completionHandler(.failure(.unknowError))
                }
                return
            }
            
            // Check status code
            guard let httpResponse = response as? HTTPURLResponse else {
                completionHandler(.failure(.connectionError))
                return
            }
            
            // Check data
            guard let receivedData = data else {
                completionHandler(.failure(.responseWithoutData))
                return
            }
            
            let networkResponse = NetworkResponse(httpResponse: httpResponse, receivedData: receivedData)
            completionHandler(.success(networkResponse))
        }).resume()
    }
}


// MARK: - Setup URL
extension URLSessionManager {
    /// Faz a criação de uma URL a partir de dados como a url base e os parâmetros que deverão ser adicionados a essa URL
    private func formatURL(service: ServiceProtocol) -> URL? {
        // Create URL
        guard var urlComponents = URLComponents(string: service.path) else {
            return nil
        }
        
        // Add Parameters
        if let receivedParameters = service.parameters {
            urlComponents.queryItems = []
            
            for parameter in receivedParameters {
                let queryItem = URLQueryItem(name: parameter.key, value: String(describing: parameter.value))
                urlComponents.queryItems?.append(queryItem)
            }
        }
        
        return urlComponents.url
    }
    
    private func setupURLRequest(url: URL, service: ServiceProtocol) -> URLRequest {
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = service.method.rawValue
        
        if let headers = service.headers {
            for header in headers {
                urlRequest.setValue(header.value, forHTTPHeaderField: header.key)
            }
        }
        
        return urlRequest
    }
}
