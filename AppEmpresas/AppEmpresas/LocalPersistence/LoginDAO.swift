//
//  LoginDAO.swift
//  AppEmpresas
//
//  Created by Isnard Silva on 23/12/20.
//

import Foundation

class LoginDAO {
    // MARK: - Properties
    private let userDefaults = UserDefaultsManager()
    
    
    // MARK: - CRUD Methods
    func saveAuthCredentials(_ authCredentials: AuthCredentials) {
        do {
            try userDefaults.saveObject(authCredentials, forKey: Identifier.DatabaseKeys.authCredentials)
        } catch {
            print(error.localizedDescription)
        }
    }
    
    func getAuthCredentials() -> AuthCredentials? {
        do {
            let authCredentials = try userDefaults.getObject(forKey: Identifier.DatabaseKeys.authCredentials, castTo: AuthCredentials.self)
            return authCredentials
        } catch {
//            print(error.localizedDescription)
            return nil
        }
    }
    
    func clearAllAuthCredentials() {
        if userDefaults.deleteObject(forKey: Identifier.DatabaseKeys.authCredentials) {
            print("Removeu tudo")
        } else {
            print("Não foi possível remover")
        }
    }
}
