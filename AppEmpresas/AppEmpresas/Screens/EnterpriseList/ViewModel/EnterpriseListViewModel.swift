//
//  EnterpriseListViewModel.swift
//  AppEmpresas
//
//  Created by Isnard Silva on 23/12/20.
//

import Foundation

class EnterpriseListViewModel {
    // MARK: - Properties
    private let enterpriseService = EnterpriseService()
    var enterprises: [Enterprise] = []
    var didReceiveEnterprises: (() -> Void)?
    var didReceiveError: ((Error) -> Void)?
}


// MARK: - Fetch and Search Methods
extension EnterpriseListViewModel {
    func searchEnterprisesByName(_ name: String) {
        enterpriseService.searchByName(name: name, completionHandler: { [weak self] result in
            switch result {
            case .failure(let error):
                self?.didReceiveError?(error)
                
            case .success(let receivedEnterprises):
                self?.enterprises = receivedEnterprises
                self?.didReceiveEnterprises?()
            }
        })
    }
}
