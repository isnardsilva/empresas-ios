//
//  EnterpriseHeaderReusableView.swift
//  AppEmpresas
//
//  Created by Isnard Silva on 24/12/20.
//

import UIKit

class EnterpriseHeaderReusableView: UICollectionReusableView {
    // MARK: - Views
    private let titleLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor(named: Identifier.Color.fontGray)
        label.font = UIFont.systemFont(ofSize: 15)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    
    // MARK: - Properties
    var title: String = String() {
        didSet {
            setupTitle()
        }
    }
    
    
    // MARK: - Initialization
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    // MARK: - Setup title
    private func setupTitle() {
        titleLabel.text = title
    }
}


// MARK: -
extension EnterpriseHeaderReusableView: ViewCodable {
    func setupHierarchy() {
        addSubview(titleLabel)
    }
    
    func setupConstraints() {
        NSLayoutConstraint.activate([
            titleLabel.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 8),
            titleLabel.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -8),
            titleLabel.bottomAnchor.constraint(equalTo: self.bottomAnchor)
        ])
    }
    
    func setupAditionalConfiguration() {
        backgroundColor = .clear
    }
}
