//
//  EnterpriseCell.swift
//  AppEmpresas
//
//  Created by Isnard Silva on 23/12/20.
//

import UIKit

class EnterpriseCell: UICollectionViewCell {
    // MARK: - Views
    private lazy var imageView: UIImageView = {
        let imageView = UIImageView(frame: self.contentView.bounds)
        return imageView
    }()
    
    
    // MARK: - Properties
    var enterpriseViewModel: EnterpriseViewModel? {
        didSet {
            setupEnterprisePhoto()
        }
    }
    
    
    // MARK: - Initialization
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.contentView.addSubview(imageView)
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    // MARK: - Setup Enterprise Content
    private func setupEnterprisePhoto() {
        guard let viewModel = enterpriseViewModel else { return }
        
        if let validURL = URL(string: viewModel.photo) {
            imageView.setImage(url: validURL)
        } else {
            imageView.image = UIImage(systemName: "photo")
        }
    }
}
