//
//  EnterpriseListViewController.swift
//  AppEmpresas
//
//  Created by Isnard Silva on 23/12/20.
//

import UIKit

class EnterpriseListViewController: UIViewController {
    // MARK: - Properties
    private lazy var baseView = EnterpriseListView()
    
    private lazy var viewModel: EnterpriseListViewModel = {
        let viewModel = EnterpriseListViewModel()
        
        // Activate Bidings
        viewModel.didReceiveEnterprises = { [weak self] in
            self?.didReceiveEnterprises()
        }
        
        viewModel.didReceiveError = { [weak self] error in
            self?.didReceiveError(error: error)
        }
        
        return viewModel
    }()
    
    private var dataSource: EnterpriseListDataSource? {
        didSet {
            guard let validDataSource = dataSource else {
                return
            }
            
            // Setup Enterprise Selection
            validDataSource.didSelectEnterprise = { [weak self] selectedEnterprise in
                self?.didSelectEnterprise(selectedEnterprise: selectedEnterprise)
            }
            
            DispatchQueue.main.async { [weak self] in
                self?.baseView.collectionView.dataSource = validDataSource
                self?.baseView.collectionView.delegate = validDataSource
                self?.baseView.collectionView.reloadData()
                
                if let isEmpty = self?.viewModel.enterprises.isEmpty, !isEmpty {
                    self?.baseView.collectionView.scrollToItem(at: IndexPath(row: 0, section: 0), at: .centeredVertically, animated: false)
                }
                self?.baseView.enableLoadingMode(false)
            }
        }
    }
    
    weak var coordinator: MainCoordinator?
    
    
    // MARK: - View Life Cycle
    override func loadView() {
        super.loadView()
        self.view = baseView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupCustomBackButton()
        
        baseView.searchBar.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
}

// MARK: Setup Back Button
extension EnterpriseListViewController {
    private func setupCustomBackButton() {
        self.navigationController?.navigationBar.backIndicatorImage = UIImage(named: Identifier.Image.backButton)
        self.navigationController?.navigationBar.backIndicatorTransitionMaskImage = UIImage(named: Identifier.Image.backButton)
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        self.navigationController?.navigationBar.tintColor = UIColor(named: Identifier.Color.customPink)
    }
}


// MARK: - Setup Search Bar
extension EnterpriseListViewController {
    private func setupSearchBar() {
        baseView.searchBar.delegate = self
    }
}

// MARK: - UISearchBarDelegate
extension EnterpriseListViewController: UISearchBarDelegate {
    func searchBarShouldBeginEditing(_ searchBar: UISearchBar) -> Bool {
        baseView.enableTopBarLargeMode(false)
        return true
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        guard let typedText = searchBar.text, !typedText.isEmpty else {
            searchBar.resignFirstResponder()
            return
        }
        
        viewModel.searchEnterprisesByName(typedText)
        baseView.enableLoadingMode(true)
        searchBar.resignFirstResponder()
    }
}


// MARK: - Handle Fetch of Enterpries
extension EnterpriseListViewController {
    private func didReceiveEnterprises() {
        baseView.enableErrorMode(false)
        dataSource = EnterpriseListDataSource(enterprises: viewModel.enterprises)
    }
    
    private func didReceiveError(error: Error) {
        
        // Check Authetication Error
        if let authenticationError = error as? AuthenticationError,
           authenticationError == .sessionExpired {

            DispatchQueue.main.async { [weak self] in
                self?.handleExpiredSessionError()
            }
        } else {
            baseView.enableErrorMode(true, errorMessage: error.localizedDescription)
        }
    }
    
    private func handleExpiredSessionError() {
        let title = "Ocorreu um problema"
        let message = "A sua sessão expirou. Faça login novamente."
        
        let alert = AlertManager().createDefaultAlert(title: title, message: message, buttonName: "Ok", completionHandler: { [weak self] in
            
            self?.coordinator?.navigateToLogin()
        })
        
        self.present(alert, animated: true, completion: nil)
    }
}

// MARK: - Handle Enterprise Selection
extension EnterpriseListViewController {
    private func didSelectEnterprise(selectedEnterprise: Enterprise) {
        coordinator?.navigateToEnterpriseDetail(enterprise: selectedEnterprise)
    }
}
