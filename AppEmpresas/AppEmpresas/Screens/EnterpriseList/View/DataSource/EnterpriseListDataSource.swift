//
//  EnterpriseListDataSource.swift
//  AppEmpresas
//
//  Created by Isnard Silva on 23/12/20.
//

import UIKit

class EnterpriseListDataSource: NSObject {
    // MARK: - Properties
    private let enterprises: [Enterprise]
    
    var didSelectEnterprise: ((Enterprise) -> Void)?
    
    
    // MARK: - Initialization
    init(enterprises: [Enterprise]) {
        self.enterprises = enterprises
        super.init()
    }
}

// MARK: - UICollectionViewDataSource
extension EnterpriseListDataSource: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return enterprises.count
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        // swiftlint:disable line_length
        guard let reusableView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: Identifier.Cell.enterpriseHeaderReusableView, for: indexPath) as? EnterpriseHeaderReusableView else {
            fatalError("Could not create new cell")
        }
        
        if enterprises.count == 1 {
            reusableView.title = "\(enterprises.count) resultado encontrados"
        } else {
            reusableView.title = "\(enterprises.count) resultados encontrados"
        }
        
        return reusableView
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Identifier.Cell.enterpriseCell, for: indexPath) as? EnterpriseCell else {
            print("Failed load cell as EnterpriseCell")
            return UICollectionViewCell()
        }
        
        let currentEnterprise = enterprises[indexPath.row]
        cell.enterpriseViewModel = EnterpriseViewModel(enterprise: currentEnterprise)
        
        return cell
    }
}


// MARK: - UICollectionViewDelegateFlowLayout
extension EnterpriseListDataSource: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let leftSectionInsets: CGFloat = 8
        let rightSectionInsets: CGFloat = 8
        
        let paddingSpace = leftSectionInsets + rightSectionInsets
        let availableWidth = collectionView.frame.width - paddingSpace
        
        return CGSize(width: availableWidth, height: availableWidth * 0.5)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 8, left: 8, bottom: 0, right: 8)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize(width: collectionView.frame.width, height: 70)
    }
}


// MARK: - UICollectionViewDelegate
extension EnterpriseListDataSource: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let selectedEnterprise = enterprises[indexPath.row]
        self.didSelectEnterprise?(selectedEnterprise)
    }
}
