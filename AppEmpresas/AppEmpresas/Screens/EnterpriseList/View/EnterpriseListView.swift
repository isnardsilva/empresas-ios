//
//  EnterpriseListView.swift
//  AppEmpresas
//
//  Created by Isnard Silva on 23/12/20.
//

import UIKit

class EnterpriseListView: UIView {
    // MARK: - Outlets
    private let topBarImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: Identifier.Image.topBarLargeEnterpriseList)
        imageView.contentMode = .scaleAspectFill
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.clipsToBounds = true
        return imageView
    }()
    
    let searchBar: UISearchBar = {
        let searchBar = UISearchBar()
        searchBar.placeholder = "Pesquisar por empresa"
        searchBar.backgroundImage = UIImage()
        searchBar.backgroundColor = UIColor(named: Identifier.Color.backgroundTextField)
        searchBar.layer.cornerRadius = 5
        searchBar.translatesAutoresizingMaskIntoConstraints = false
        return searchBar
    }()
    
    lazy var collectionView: UICollectionView = {
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: UICollectionViewFlowLayout())
        collectionView.backgroundColor = .clear
        collectionView.register(UICollectionViewCell.self, forCellWithReuseIdentifier: Identifier.Cell.enterpriseCell)
        collectionView.register(EnterpriseCell.self, forCellWithReuseIdentifier: Identifier.Cell.enterpriseCell)
        // swiftlint:disable line_length
        collectionView.register(EnterpriseHeaderReusableView.self, forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: Identifier.Cell.enterpriseHeaderReusableView)
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        return collectionView
    }()
    
    private let activityIndicadorView: UIActivityIndicatorView = {
        let activityIndicador = UIActivityIndicatorView(style: .medium)
        activityIndicador.translatesAutoresizingMaskIntoConstraints = false
        return activityIndicador
    }()
    
    private let errorMessageLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor(named: Identifier.Color.fontGray)
        label.font = UIFont.systemFont(ofSize: 15)
        label.numberOfLines = 0
        label.textAlignment = .center
        label.isHidden = true
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    
    // MARK: - Constraints
    private var topBarHeightAnchorConstraint: NSLayoutConstraint!
    
    
    // MARK: - Initialization
    init() {
        super.init(frame: .zero)
        setupViews()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}


// MARK: - ViewCodable
extension EnterpriseListView: ViewCodable {
    func setupHierarchy() {
        addSubview(topBarImageView)
        addSubview(collectionView)
        addSubview(searchBar)
        addSubview(activityIndicadorView)
        addSubview(errorMessageLabel)
    }
    
    func setupConstraints() {
        topBarHeightAnchorConstraint = topBarImageView.heightAnchor.constraint(equalTo: self.heightAnchor, multiplier: 0.30)
        
        NSLayoutConstraint.activate([
            topBarImageView.topAnchor.constraint(equalTo: self.topAnchor),
            topBarImageView.leadingAnchor.constraint(equalTo: self.leadingAnchor),
            topBarImageView.trailingAnchor.constraint(equalTo: self.trailingAnchor),
            topBarHeightAnchorConstraint
        ])
        
        NSLayoutConstraint.activate([
            searchBar.centerYAnchor.constraint(equalTo: topBarImageView.bottomAnchor),
            searchBar.leadingAnchor.constraint(equalTo: self.layoutMarginsGuide.leadingAnchor),
            searchBar.trailingAnchor.constraint(equalTo: self.layoutMarginsGuide.trailingAnchor)
        ])
        
        NSLayoutConstraint.activate([
            collectionView.topAnchor.constraint(equalTo: topBarImageView.bottomAnchor),
            collectionView.leadingAnchor.constraint(equalTo: self.leadingAnchor),
            collectionView.trailingAnchor.constraint(equalTo: self.trailingAnchor),
            collectionView.bottomAnchor.constraint(equalTo: self.bottomAnchor)
        ])
        
        NSLayoutConstraint.activate([
            activityIndicadorView.centerXAnchor.constraint(equalTo: collectionView.centerXAnchor),
            activityIndicadorView.centerYAnchor.constraint(equalTo: collectionView.centerYAnchor)
        ])
        
        NSLayoutConstraint.activate([
            errorMessageLabel.leadingAnchor.constraint(equalTo: self.layoutMarginsGuide.leadingAnchor),
            errorMessageLabel.trailingAnchor.constraint(equalTo: self.layoutMarginsGuide.trailingAnchor),
            errorMessageLabel.centerYAnchor.constraint(equalTo: collectionView.centerYAnchor)
        ])
    }
    
    func setupAditionalConfiguration() {
        backgroundColor = .white
        
        if let textField = searchBar.value(forKey: "searchField") as? UITextField {
            textField.backgroundColor = UIColor(named: Identifier.Color.backgroundTextField)
        }
    }
}


// MARK: - Setup Behavior of Elements
extension EnterpriseListView {
    func enableTopBarLargeMode(_ enable: Bool) {
        // Set new background image
        let imageName = enable ? Identifier.Image.topBarLargeEnterpriseList : Identifier.Image.topBarShortEnterpriseList
        let newImage = UIImage(named: imageName)
        topBarImageView.image = newImage
        
        
        // Set new height
        let heightMultiplier: CGFloat = enable ? 0.3 : 0.12
        topBarHeightAnchorConstraint.isActive = false
        topBarHeightAnchorConstraint = topBarImageView.heightAnchor.constraint(equalTo: self.heightAnchor, multiplier: heightMultiplier)
        topBarHeightAnchorConstraint.isActive = true
    }
    
    func enableLoadingMode(_ enable: Bool) {
        collectionView.isHidden = enable
        errorMessageLabel.isHidden = true
        activityIndicadorView.isHidden = !enable
        
        if enable {
            activityIndicadorView.startAnimating()
        } else {
            activityIndicadorView.stopAnimating()
        }
    }
    
    func enableErrorMode(_ enable: Bool, errorMessage: String = "Ocorreu um erro inesperado.") {
        DispatchQueue.main.async { [weak self] in
            self?.collectionView.isHidden = enable
            self?.activityIndicadorView.isHidden = enable
            
            self?.errorMessageLabel.text = errorMessage
            self?.errorMessageLabel.isHidden = !enable
        }
    }
}
