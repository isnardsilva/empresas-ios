//
//  EnterpriseViewModel.swift
//  AppEmpresas
//
//  Created by Isnard Silva on 23/12/20.
//

import Foundation

class EnterpriseViewModel {
    // MARK: - Properties
    private let enterprise: Enterprise
    
    var id: Int {
        return enterprise.id
    }
    
    var name: String {
        return enterprise.name
    }
    
    var photo: String {
        return IoasysAPISources.baseURL + enterprise.photo
    }
    
    var description: String {
        return enterprise.description
    }
    
    var city: String {
        return enterprise.city
    }
    
    var country: String {
        return enterprise.country
    }
    
    var value: Double {
        return enterprise.value
    }
    
    var sharePrice: Double {
        return enterprise.sharePrice
    }
    
    
    // MARK: - Initialization
    init(enterprise: Enterprise) {
        self.enterprise = enterprise
    }
}
