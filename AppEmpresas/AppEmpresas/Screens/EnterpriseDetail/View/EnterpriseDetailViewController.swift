//
//  EnterpriseDetailViewController.swift
//  AppEmpresas
//
//  Created by Isnard Silva on 23/12/20.
//

import UIKit

class EnterpriseDetailViewController: UIViewController {
    // MARK: - Properties
    private lazy var baseView = EnterpriseDetailView()
    private let viewModel: EnterpriseViewModel
    
    weak var coordinator: MainCoordinator?
    
    
    // MARK: - Initialization
    init(enterprise: Enterprise) {
        self.viewModel = EnterpriseViewModel(enterprise: enterprise)
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    // MARK: - View Life Cycle
    override func loadView() {
        super.loadView()
        self.view = baseView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupNavigationBar()
        setupEnterpriseData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: false)
    }
}


// MARK: - Setup Navigation Bar
extension EnterpriseDetailViewController {
    private func setupNavigationBar() {
        navigationItem.title = viewModel.name
        
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
    }
}


// MARK: - Setup Data
extension EnterpriseDetailViewController {
    private func setupEnterpriseData() {
        // Set Photo
        if let photoURL = URL(string: viewModel.photo) {
            baseView.photoImageView.setImage(url: photoURL)
        } else {
            baseView.photoImageView.image = UIImage(systemName: "photo")
        }
        
        // Set Description
        baseView.descriptionLabel.text = viewModel.description
    }
}
