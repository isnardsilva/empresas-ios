//
//  LoginView.swift
//  AppEmpresas
//
//  Created by Isnard Silva on 22/12/20.
//

import UIKit

class LoginView: UIView {
    // MARK: - Views
    // Top Bar
    private let topBarImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: Identifier.Image.topBarLogin)
        imageView.contentMode = .scaleAspectFill
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    private let topBarContentStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.spacing = 16
        stackView.translatesAutoresizingMaskIntoConstraints = false
        return stackView
    }()
    
    private let logoImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: Identifier.Image.logoWithoutName)
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    private let titleLabel: UILabel = {
        let label = UILabel()
        label.textColor = .white
        label.text = "Seja bem vindo ao empresas!"
        return label
    }()
    
    
    // Inputs
    private let inputsStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .vertical
        stackView.spacing = 40
        stackView.distribution = .fill
        return stackView
    }()
    
    let emailTextField: CustomTextField = {
        let textField = CustomTextField()
        textField.keyboardType = .emailAddress
        textField.autocapitalizationType = .none
        textField.placeholderLabel.text = "E-mail"
        return textField
    }()
    
    let passwordTextField: CustomTextField = {
        let textField = CustomTextField()
        textField.placeholderLabel.text = "Senha"
        textField.isSecureTextEntry = true
        return textField
    }()
    
    let errorMessageLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .right
        label.font = UIFont.systemFont(ofSize: 15)
        label.textColor = .red
        label.isHidden = true
        label.numberOfLines = 0
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let signInButton: UIButton = {
        let button = UIButton()
        button.setTitle("Entrar".uppercased(), for: .normal)
        button.backgroundColor = UIColor(named: Identifier.Color.customPink)
        button.layer.cornerRadius = 5
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    
    // MARK: - Constraints
    private var topBarTopAnchorConstraint: NSLayoutConstraint!
    private var topBarCenterYAnchorContentStackView: NSLayoutConstraint!
    
    
    // MARK: - Initialization
    init() {
        super.init(frame: .zero)
        setupViews()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

// MARK: - ViewCodable
extension LoginView: ViewCodable {
    func setupHierarchy() {
        // Top Bar
        addSubview(topBarImageView)
        
        // Top Bar Content
        addSubview(topBarContentStackView)
        topBarContentStackView.addArrangedSubview(logoImageView)
        topBarContentStackView.addArrangedSubview(titleLabel)
        
        // Inputs
        addSubview(inputsStackView)
        inputsStackView.addArrangedSubview(emailTextField)
        inputsStackView.addArrangedSubview(passwordTextField)
        
        // Error Message Label
        addSubview(errorMessageLabel)
        
        // Submit Button
        addSubview(signInButton)
    }
    
    func setupConstraints() {
        topBarTopAnchorConstraint = topBarImageView.topAnchor.constraint(equalTo: self.topAnchor)
        
        NSLayoutConstraint.activate([
            topBarTopAnchorConstraint,
            topBarImageView.leadingAnchor.constraint(equalTo: self.leadingAnchor),
            topBarImageView.trailingAnchor.constraint(equalTo: self.trailingAnchor),
            topBarImageView.heightAnchor.constraint(equalTo: self.heightAnchor, multiplier: 0.30)
        ])
        
        topBarCenterYAnchorContentStackView = topBarContentStackView.centerYAnchor.constraint(equalTo: topBarImageView.centerYAnchor)
        
        NSLayoutConstraint.activate([
            topBarContentStackView.centerXAnchor.constraint(equalTo: topBarImageView.centerXAnchor),
            topBarCenterYAnchorContentStackView
        ])
        
        
        NSLayoutConstraint.activate([
            inputsStackView.topAnchor.constraint(equalTo: topBarImageView.bottomAnchor, constant: 70),
            inputsStackView.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 16),
            inputsStackView.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -16)
        ])
        
        NSLayoutConstraint.activate([
            errorMessageLabel.topAnchor.constraint(equalTo: inputsStackView.bottomAnchor, constant: 8),
            errorMessageLabel.leadingAnchor.constraint(equalTo: inputsStackView.leadingAnchor),
            errorMessageLabel.trailingAnchor.constraint(equalTo: inputsStackView.trailingAnchor)
        ])
        
        NSLayoutConstraint.activate([
            signInButton.topAnchor.constraint(equalTo: self.errorMessageLabel.bottomAnchor, constant: 30),
            signInButton.centerXAnchor.constraint(equalTo: self.centerXAnchor),
            signInButton.heightAnchor.constraint(equalToConstant: 48),
            signInButton.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 0.8)
        ])
    }
    
    func setupAditionalConfiguration() {
        backgroundColor = .white
    }
}

// MARK: - Setup element behavior
extension LoginView {
    /// Habilita/Desabilita os elementos de Login, tal como, Text Fields e Buttons
    func enableLoginFields(_ isEnable: Bool) {
        DispatchQueue.main.async { [weak self] in
            self?.emailTextField.isEnabled = isEnable
            self?.passwordTextField.isEnabled = isEnable
            self?.signInButton.isEnabled = isEnable
        }
    }
    
    func enableInputMode(_ enable: Bool) {
        // Hide title
        titleLabel.isHidden = enable
        
        topBarTopAnchorConstraint.constant = enable ? -70 : 0
        topBarCenterYAnchorContentStackView.constant = enable ? 50 : 0
    }
}
