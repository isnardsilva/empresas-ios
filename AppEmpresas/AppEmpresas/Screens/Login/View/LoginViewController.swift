//
//  LoginViewController.swift
//  AppEmpresas
//
//  Created by Isnard Silva on 22/12/20.
//

import UIKit

/// Tela de autenticação do aplicativo
class LoginViewController: UIViewController {
    // MARK: - Properties
    private lazy var baseView = LoginView()
    private let loadingAlert = LoadingAlertViewController()
    
    private lazy var viewModel: LoginViewModel = {
        let viewModel = LoginViewModel()
        
        viewModel.didLoginSuccess = { [weak self] in
            self?.didLoginSuccess()
        }
        
        viewModel.didLoginFailure = { [weak self] error in
            self?.didLoginFailure(error: error)
        }
        
        return viewModel
    }()
    
    weak var coordinator: MainCoordinator?
    
    
    
    // MARK: - View Life Cycle
    override func loadView() {
        super.loadView()
        self.view = baseView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupActions()
        setupKeyboardEvents()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
}


// MARK: - Actions
extension LoginViewController {
    private func setupActions() {
        baseView.signInButton.addTarget(self, action: #selector(signInButtonTouched(_:)), for: .touchUpInside)
    }
    
    @objc private func signInButtonTouched(_ sender: UIButton) {
        baseView.enableLoginFields(false)
        
        self.present(loadingAlert, animated: true, completion: nil)
        
        let typedEmail = baseView.emailTextField.text ?? ""
        let typedPassword = baseView.passwordTextField.text ?? ""

        let loginWithEmail = LoginWithEmail(email: typedEmail, password: typedPassword)
        viewModel.authenticate(loginMethod: loginWithEmail)
    }
}


// MARK: - Handle Keyboard Events
extension LoginViewController {
    private func setupKeyboardEvents() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillDisappear), name: UIResponder.keyboardWillHideNotification, object: nil)
        
            NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillAppear), name: UIResponder.keyboardWillShowNotification, object: nil)
    }
    
    @objc func keyboardWillAppear() {
        baseView.enableInputMode(true)
    }

    @objc private func keyboardWillDisappear() {
        baseView.enableInputMode(false)
    }
}


// MARK: - Handle Login Failures and Successes
extension LoginViewController {
    private func didLoginSuccess() {
        baseView.enableLoginFields(true)
        
        DispatchQueue.main.async { [weak self] in
            self?.loadingAlert.dismiss(animated: true, completion: {
                self?.coordinator?.navigateToEnterpriseList()
            })
        }
    }
    
    private func didLoginFailure(error: Error) {
        baseView.enableLoginFields(true)
        
        DispatchQueue.main.async { [weak self] in
            self?.loadingAlert.dismiss(animated: true, completion: nil)
            
            self?.baseView.errorMessageLabel.isHidden = false
            self?.baseView.errorMessageLabel.text = error.localizedDescription
        }
    }
}
