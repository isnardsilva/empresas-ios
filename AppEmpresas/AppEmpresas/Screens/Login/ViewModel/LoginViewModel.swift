//
//  LoginViewModel.swift
//  AppEmpresas
//
//  Created by Isnard Silva on 23/12/20.
//

import Foundation

class LoginViewModel {
    // MARK: - Properties
    private let loginDAO = LoginDAO()
    
    var didLoginSuccess: (() -> Void)?
    var didLoginFailure: ((Error) -> Void)?
    
    
    // MARK: - Authenticate Method
    func authenticate(loginMethod: LoginProtocol) {
        loginMethod.authenticate(completionHandler: { [weak self] result in
            switch result {
            case .failure(let error):
                self?.didLoginFailure?(error)
                
            case .success(let authCredentials):
                self?.loginDAO.saveAuthCredentials(authCredentials)
                self?.didLoginSuccess?()
            }
        })
    }
}
