//
//  AuthCredentials.swift
//  AppEmpresas
//
//  Created by Isnard Silva on 23/12/20.
//

import Foundation

/// Informações que permitem que o usuário se mantenha conectado no aplicativo
struct AuthCredentials: Codable {
    let accessToken: String
    let uid: String
    let client: String
}
