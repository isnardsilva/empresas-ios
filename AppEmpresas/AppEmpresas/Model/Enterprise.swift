//
//  Enterprise.swift
//  AppEmpresas
//
//  Created by Isnard Silva on 23/12/20.
//

import Foundation

struct Enterprise: Codable {
    // MARK: - Properties
    let id: Int
    let name: String
    let photo: String
    let description: String
    let city: String
    let country: String
    let value: Double
    let sharePrice: Double
    
    
    // MARK: - Parse
    enum CodingKeys: String, CodingKey {
        case id, photo, description, city, country, value
        case name = "enterprise_name"
        case sharePrice = "share_price"
    }
}
